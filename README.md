# Module of WainWorkEngine

Dependent on:
[WainWorkEngine.Extensions] v1.0.0+

## Getting started

Add this to your Packages\manifest.json
```
"com.wainwork.engine.sounds": "https://gitlab.com/wainworkengine/sounds.git",
"com.wainwork.engine.extensions": "https://gitlab.com/wainworkengine/extensions.git",
```

## How to use

Обязаельно вызывать Update у SoundManager. В Update проверяется завершение аудиклипа, чтобы перевести контроллер в idle режим.

Example:
```
public interface ISoundManager : WainWork.Sounds.ISoundManager<WainWork.Sounds.IAudioController>
```

```
public class LocalSoundManager : SoundManager<IAudioControllerElement>, ISoundManager
public class LocalSoundManager : SoundManager3D, ISoundManager
```
